import { useState, useEffect } from 'react';

export default function useCounter(options) {
  const step = (options && options.step) || options || 1;
  const min = (options && options.min) || 0;
  const timeInterval = (options && options.timeInterval) || 1000;

  const [counter, setCounter] = useState(min);

  useEffect(() => {
    const intervalId = setInterval(() => {
      setCounter((prevCounter) => prevCounter + step);
    }, timeInterval);

    return () => clearInterval(intervalId);
  }, [step, timeInterval]);

  return counter;
};